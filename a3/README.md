> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Enrique Toledo

### Assignment 3 Requirements:

*Three  Parts:*

1. Backwards engineer Painting Estimator application
2. Skill sets screenshots
3. Chapter Questions (Ch 6) 

#### README.md file should include the following items:

* Screenshot of a3_painting estimator application running
* Link to A3 .ipynb file: [painting_estimator.ipynb](a3_painting_estimator/painting_estimator.ipynb "A3 Jupyter Notebook")
* Screenshot of Skill Set 4 - Calorie Percentage
* Screenshot of Skill Set 5 - Python Selection Structures
* Screenshot of Skill Set 6 - Python Loops

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

### Assignment Screenshots

| Painting Estimator                                           |
|--------------------------------------------------------------|
| ![Painting Estimator screenshot](img/painting_estimator.png) |

| Skill set 4 - Calorie Percentage                          | Skill set 5 - Python Selection Structures                          | Skill set 6 - Python Loops                          |
|-----------------------------------------------------------|--------------------------------------------------------------------|-----------------------------------------------------|
| ![skill set 4 screenshot](img/ss4_calorie_percentage.png) | ![skill set 5 screenshot](img/ss5_python_selection_structures.png) | ![skill set 6 screenshot](img/ss6_python_loops.png) |
                                                                                      
| a3_painting_estimator.ipynb                                                   |                                                                             |
|-------------------------------------------------------------------------------|-----------------------------------------------------------------------------|
| ![Painting estimator jupyter screenshot_1](img/a3_painting_estimator_1.png)   | ![Painting estimator jupyter screenshot_2](img/a3_painting_estimator_2.png) |



