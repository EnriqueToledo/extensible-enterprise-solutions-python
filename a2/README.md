> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Enrique Toledo

### Assignment 2 Requirements:

*Three  Parts:*

1. Payroll application
2. Skill sets screenshots
3. Chapter Questions (Ch 4) 

#### README.md file should include the following items:

* Screenshot of a2_payroll application running
* Link to A2 .ipynb file: [payroll.ipynb](a2_payroll/payroll.ipynb "A2 Jupyter Notebook")
* Screenshot of Skill Set 1 - Square Feet to Acres
* Screenshot of Skill Set 2 - Miles Per Gallon 
* Screenshot of Skill Set 3 - IT/ICT Student Percentage

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

| Payroll No Overtime                                            | Payroll With Overtime                                              |
|----------------------------------------------------------------|--------------------------------------------------------------------|
| ![Payroll No Overtime screenshot](img/payroll_no_overtime.png) | ![Payroll With Overtime screenshot](img/payroll_with_overtime.png) |

| Skill Set 1 - Square Feet to Acres                        | Skill Set 2 - Miles Per Gallon                          | Skill Sets 3 - IT/ICT Student Percentage                         |
|-----------------------------------------------------------|---------------------------------------------------------|------------------------------------------------------------------|
| ![Skill Set 1 screenshot](img/ss1_square_ft_to_acres.png) | ![Skill Set 2 screenshot](img/ss2_miles_per_gallon.png) | ![Skill Set 3 screenshot](img/ss3_it_ict_student_percentage.png) |
                                                                                      
| a2_payroll.ipynb                                               |                                                       |
|----------------------------------------------------------------|-------------------------------------------------------|
| ![Payroll jupyter screenshot_1](img/a2_payroll_1.png)          | ![Payroll jupyter screenshot_2](img/a2_payroll_2.png) |



