> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Enrique Toledo

### Project 2 Requirements:

*Four Parts:*

1. Backward-engineer the lis4369_p2_requirements.txt file
2. Be sure to include at least two plots (*must* include *your* name in plot titles), in your README.md file. 
3. Test program using RStudio
4. Answer questions (R language)

#### README.md file should include the following items:

* Screenshot of at least two plots
* Screenshot of Soads.R code
* Screenshot of requirement.txt file
* Screenshot of plots from soads.R output

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

### Assignment Screenshots

| Requirements.txt                |
|---------------------------------|
| ![Requirements.txt](img/p2.png) | 

| Displacement vs MPG plot                    | Weight vs MPG plot                    |
|---------------------------------------------|---------------------------------------|
| ![displacement vs mpg plot](img/plot_1.png) | ![weight vs mpg plot](img/plot_2.png) |

| soads.R                   |
|---------------------------|
| ![Soads.R](img/soads.png) |   

| soads.R Output                              |                                             |
|---------------------------------------------|---------------------------------------------|
| ![soads.R output_1](img/soads_output_1.png) | ![soads.R output_2](img/soads_output_2.png) |

