> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Enrique Toledo

### Project 1 Requirements:

*Five Parts:*

1. Code and run demo.py to backwards engineer data analysis 1 screenshot
2. Update conda and install Python packages
3. Provide screesnshots of backwards engineered app
4. Provide screenshots of skill sets 
5. Chapter Questions (Ch 7 & 8) 

#### README.md file should include the following items:

* Screenshot of p1_data_analysis application running
* Link to P1 .ipynb file: [p1_data_analysis.ipynb](p1_data_analysis_1/p1_data_analysis.ipynb "P1 Jupyter Notebook")
* Screenshot of Skill Set 7 - Using Lists
* Screenshot of Skill Set 8 - Using Tuples
* Screenshot of Skill Set 9 - Using Sets

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

### Assignment Screenshots

| P1_data_analysis1 screenshots                    |                                                  |
|--------------------------------------------------|--------------------------------------------------|
| ![jupyter screenshot_1](img/jup_screenshot1.png) | ![jupyter screenshot_2](img/jup_screenshot2.png) | 
|--------------------------------------------------|--------------------------------------------------|
| ![jupyter screenshot_3](img/jup_screenshot3.png) | ![jupyter screenshot_4](img/jup_screenshot4.png) |

| Data Analysis1 Scheenshots                              |                                                    |
|---------------------------------------------------------|----------------------------------------------------|
| ![p1_data_analysis1_screenshot1](img/p1_screenshot.png) | ![p1_data_analysis1_screenshot2](img/p1_graph.png) |

| Skill set 7 - Using Lists              | Skill set 8 - Using Tuples             |
|----------------------------------------|----------------------------------------|
| ![Skill set 7](img/ss7_screenshot.png) | ![Skill set 8](img/ss8_screenshot.png) |

| Skill set 9 - Using Sets               |
|----------------------------------------|
| ![Skill set 9](img/ss9_screenshot.png) |


