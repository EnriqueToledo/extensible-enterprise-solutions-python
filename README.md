> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>

# LIS4369 - Extensible Enterprise Solutions (Python)

## Enrique Toledo

## LIS4369 Requirements

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code
    - Create *a1_tip_calculator* application
    - Create *a1 tip calculator* Jupyter Notebook
    - Provide Screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Payroll app using "Separation of Concerns" design principles
    - Provide screenshots of completed app
    - Provide screenshots of completed Python skill sets

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Backwards engineer painting estimator application screenshot using separation of concerns
    - Provide screenshots of completed app
    - Provide screenshots of completed Python skill sets

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Code and run demo.py.
    - Test Python Package installer
    - Research how to install missing packages
    - Create at least three functions that are called by the program
    - Display graph as per instructions with demo.py
    - Provide screenshots for skills sets 

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Complete the following tutorial: Introduction_to_R_Setup_and_TutorialSave as: learn_to_use_r.R
    - Code and run lis4369_a5.R
    - Be sure to include at least two plotsfor each part—that is, at least two plots for the tutorial, and two plots for the assignment file
 
6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Code and run demo.py to backwards engineer data analysis 1 screenshot
    - Update conda and install Python packages
    - Provide screesnshots of backwards engineered app
    - Provide screenshots of skill sets

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Backward-engineer the lis4369_p2_requirements.txt file
    - Be sure to include at least two plots (*must* include *your* name in plot titles), in your README.md file.
    - Test program using RStudio
