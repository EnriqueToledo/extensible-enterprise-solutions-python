> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Enrique Toledo

### Assignment 5 Requirements:

*Seven Parts:*

1. Complete the following tutorial: Introduction_to_R_Setup_and_TutorialSave as: learn_to_use_r.R
2. Code and run lis4369_a5.R
3. Be sure to include at least two plotsfor each part—that is, at least two plots for the tutorial, and two plots for the assignment file
4. R Commands:save a file of all the R commands included in the tutorial
5. R Console:save a screenshot of some of the R commands executed above
6. Graphs:save at least2separate image filesdisplaying graph plotscreated from the tutorial
7. RStudio:save onescreenshot, displaying the following 4 windows:
    - R source code(top-left corner)
    - Console(bottom-left corner)
    - Environment(or History), (top-right corner)
    - Plots(bottom-right corner)

#### README.md file should include the following items:

* Screenshots of 2 plots from learn_to_use_r.R
* Screenshots of 2 plots from lis4369_a5.R
* Screenshot of R studio displaying 4 windows 
* Screenshot of Skill Set 13 - Sphere Volume Calculator
* Screenshot of Skill Set 14 - Calculator with Error Handling
* Screenshot of Skill Set 15 - File Write/Read

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

### Assignment Screenshots

| Learn to use R screenshot                                      |
|----------------------------------------------------------------|
| ![Learn to use R screenshot](img/a5_assignment_screenshot.png) |

| Learn to use R graphs                       |                                            |
|---------------------------------------------|--------------------------------------------|
| ![a5 graph1](img/a5_assignment_graph_1.png) |![a5 graph2](img/a5_assignment_graph_2.png) |

| Demo Graphs                       |                                           |
|-----------------------------------|-------------------------------------------|
| ![Demo graph 1](img/demo_graph_2.png) | ![Demo graph 2](img/demo_graph_1.png) |

| Skill Set 13 - Sphere Volume Calculator    |
|--------------------------------------------|
| ![Skill set 13 screenshot](img/ss13.png)   |

| Skill Set 14 - Calculator with Error Handling   |                                              |
|-------------------------------------------------|----------------------------------------------|
| ![Skill set 14 screenshot](img/ss14_1.png)      | ![Skill set 14_2 screenshot](img/ss14_2.png) |

| Skill Set 15 - File Write/Read           |
|------------------------------------------|
| ![Skill set 15 screenshot](img/ss15.png) |

