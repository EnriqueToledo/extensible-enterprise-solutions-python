def get_requirements():
    print("Python Dictionaries")
    print("Developer: Enrique Toledo")
    print("\nProgram Requirements\n"
    + "\n1. Dictionaries (Python data structure): unordered key:value pairs."
    + "\n2. Dictionary: an associative array (also known as hashes)."
    + "\n3. Any key in a dictionary is associated (or mapped) to a value (i.e., any Python data type)."
    + "\n4. Keys must be of immutable type (string, number or tuple with immutable elements) and must be unique."
    + "\n5. Values: can be any data type and can repeat."
    + "\n6. Create a program that mirrors the folowing IPO (input/process/output) format.\n"
    + "\ta. Create empty dictionary, using curly braces {}: my_dictionary = {}\n"
    + "\tb. Use the following keys: fname, lname, degree, major, gpa"
    + "\nNote: Dictionaries have key-value pairs instead of single values; this differentiates a dictioanry from a set.")

def myinput():
    print("Input:")
    fname = str(input("First Name: "))
    lname = str(input("Last Name: "))
    degree = str(input("Degree: "))
    major = str(input("Major (IT or ICT): "))
    gpa = str(input("GPA: "))
    thisdict = {"fname" : fname,
                "lname" : lname,
                "degree" : degree,
                "major" : major,
                "gpa" : gpa }
    return thisdict

def output(thisdict):
    print("\nOutput:")
    print("Print my_dictionary:")
    print(thisdict)

    print("\nReturn view of dictionary's (key, value) pair, built-in function: ")
    temp = thisdict.items()
    print(temp)

    print("\nReturn view object of all values in dictionary, built-in function: ")
    temp = thisdict.values()
    print(temp)

    print("\nPrint only first and last names, using keys: ")
    print(thisdict["fname"] + " " + thisdict["lname"])

    print("\nPrint onlu first and last names, using get() function: ")
    print(thisdict.get("fname") + " " + thisdict.get("lname"))

    print("\nCount number of items (key:value pairs) in dictionary")
    print(len(thisdict))

    print("\nRemove last dictionary item (popitem): ")
    thisdict.popitem()

    print(thisdict)
    print("\nDelete major from dictionary, using keys: ")
    thisdict.pop("major")
    print(thisdict)

    print("\nReturn object type: ")
    print(type(thisdict))

    print("\nDelete all items from this list: ")
    thisdict.clear()

    print(thisdict)