> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Enrique Toledo

### Assignment 4 Requirements:

*Seven Parts:*

1. Code and run demo.py.
2. Test Python Package installer                           
3. Research how to install missing packages
4. Create at least three functions that are called by the program
5. Display graph as per instructions with demo.py
6. Provide screenshots for skills sets
7. Chapter Questions (Ch 9 & 10) 

#### README.md file should include the following items:

* Screenshot of a4_data_analysis_2 application running
* Link to a4 .ipynb file: [a4_data_analysis_2.ipynb](data_analysis_2/a4_data_analysis_2.ipynb "A4 Jupyter Notebook")
* Screenshot of Skill Set 10 - Using Dictionaries
* Screenshot of Skill Set 11 - Random Number Generator
* Screenshot of Skill Set 12 - Temperature Conversion Program

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

### Assignment Screenshots

| Data Analysis 2 Screenshots                   |                                               |
|-----------------------------------------------|-----------------------------------------------|
| ![data_analysis_2](img/data_screenshot_1.png) | ![data_analysis_2](img/data_screenshot_2.png) |


| Data Analysis1 Scheenshots |
|----------------------------|
| ![a4_graph](img/graph.png) |

| Jupyter notebook screenshot 1                     | Jupiter notebook screenshot 2                     |
|---------------------------------------------------|---------------------------------------------------|
| ![jupyter_screenshot](img/jupyter_notebook_1.png) | ![jupyter_screenshot](img/jupyter_notebook_2.png) |

| Jupyter notebook screenshot 3                     | Jupiter notebook screenshot 4                     |
|---------------------------------------------------|---------------------------------------------------|
| ![jupyter_screenshot](img/jupyter_notebook_3.png) | ![jupyter_screenshot](img/jupyter_notebook_4.png) |

| Skill set 10 - Using Dictionaries | Skill set 11 - Random Number Generator | Skill set 12 - Temperature Conversion Program |
|-----------------------------------|----------------------------------------|-----------------------------------------------|
| ![ss10](img/ss10.png)             | ![ss11](img/ss11.png)                  | ![ss12](img/ss12.png)                         |

